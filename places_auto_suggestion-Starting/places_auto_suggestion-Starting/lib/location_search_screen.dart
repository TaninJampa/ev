import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:places_auto_suggestion/components/network_utility.dart';
import 'package:places_auto_suggestion/models/autocomplate_prediction.dart';
import 'package:places_auto_suggestion/models/place_auto_complate_response.dart';
import 'components/location_list_tile.dart';
import 'constants.dart';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;

class SearchLocationScreen extends StatefulWidget {
  const SearchLocationScreen({Key? key}) : super(key: key);

  @override
  State<SearchLocationScreen> createState() => _SearchLocationScreenState();
}

class _SearchLocationScreenState extends State<SearchLocationScreen> {
  List<AutocompletePrediction> placePrediction = [];

  void placeAutocomplete(String query) async {
    Uri uri =
        Uri.https("maps.googleapis.com", 'maps/api/place/autocomplete/json', {
      "input": query,
      "location": '7.00836,100.47668',
      "radius": '1000',
      "language": 'th',
      "types": 'establishment',
      "key": apiKey,
    });
    String? response = await NetworkUtility.fetchUrl(uri);

    if (response != null) {
      PlaceAutocompleteResponse result =
          PlaceAutocompleteResponse.parseAutocompleteResult(response);
      if (result.predictions != null) {
        setState(() {
          placePrediction = result.predictions!;
        });
      }
    }
  }

  Future<Map<String, dynamic>> getPlace(String? input) async {
    final String url =
        'https://maps.googleapis.com/maps/api/place/details/json?place_id=$input&key=$apiKey';

    var response = await http.get(Uri.parse(url));

    var json = convert.jsonDecode(response.body);

    var results = json['result'] as Map<String, dynamic>;

    return results;
  }

  void gotoSearchedPlace(double lat, double lng) {}

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   leading: Padding(
      //     padding: const EdgeInsets.only(left: defaultPadding),
      //     child: CircleAvatar(
      //       backgroundColor: secondaryColor10LightTheme,
      //       child: SvgPicture.asset(
      //         "assets/icons/location.svg",
      //         height: 16,
      //         width: 16,
      //         color: secondaryColor40LightTheme,
      //       ),
      //     ),
      //   ),
      //   title: const Text(
      //     "Set Delivery Location",
      //     style: TextStyle(color: textColorLightTheme),
      //   ),
      //   actions: [
      //     CircleAvatar(
      //       backgroundColor: secondaryColor10LightTheme,
      //       child: IconButton(
      //         onPressed: () {},
      //         icon: const Icon(Icons.close, color: Colors.black),
      //       ),
      //     ),
      //     const SizedBox(width: defaultPadding)
      //   ],
      // ),
      body: Column(
        children: [
          Form(
            child: Padding(
              padding: const EdgeInsets.only(top: 64),
              child: TextFormField(
                onChanged: (value) {
                  placeAutocomplete(value);
                },
                textInputAction: TextInputAction.search,
                decoration: const InputDecoration(
                  hintText: "Search your location",
                  prefixIcon: Padding(
                    padding: EdgeInsets.symmetric(vertical: 12),
                    child: Icon(Icons.assistant_direction),
                    // child: SvgPicture.asset(
                    //   "assets/icons/location_pin.svg",
                    //   color: secondaryColor40LightTheme,
                    // ),
                  ),
                ),
              ),
            ),
          ),
          const Divider(
            height: 4,
            thickness: 4,
            color: secondaryColor5LightTheme,
          ),
          Padding(
            padding: const EdgeInsets.all(defaultPadding),
            child: ElevatedButton.icon(
              onPressed: () {
                placeAutocomplete("Dubai");
              },
              icon: SvgPicture.asset(
                "assets/icons/location.svg",
                height: 16,
              ),
              label: Text(""),
              style: ElevatedButton.styleFrom(
                backgroundColor: secondaryColor10LightTheme,
                foregroundColor: textColorLightTheme,
                elevation: 0,
                fixedSize: const Size(double.infinity, 40),
                shape: const RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                ),
              ),
            ),
          ),
          const Divider(
            height: 4,
            thickness: 4,
            color: secondaryColor5LightTheme,
          ),
          Expanded(
            child: ListView.builder(
              itemCount: placePrediction.length,
              itemBuilder: (context, index) => LocationListTile(
                press: () async {
                  var place = await getPlace(placePrediction[index].placeId);
                  gotoSearchedPlace(place['geometry']['location']['lat'],
                      place['geometry']['location']['lng']);
                  print(place['geometry']['location']['lat']);
                  print(place['geometry']['location']['lng']);
                },
                location: placePrediction[index].description!,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
